# import rospy
# from geometry_msgs.msg import PoseArray, Twist, Pose
# from swarming.msg import VelocityArray

from math import *
from random import *
import time
import cv2  # OpenCV2 for saving an image
import numpy as np
import matplotlib.pyplot as plt
from multiprocessing import Pool


frequency = 0
initial_time = time.time()

is_real = True
show_on = True

Num_from = 50
Num_to = 150

Temp_from = 0.2
Temp_to = 1.8

sample_T = 50
sample_N = 50

simulation_steps = 50000
dt = 0.05


temp_a = np.float32
temp_r = np.float32

MAG = 3


class Swarm:
    def __init__(self):

        ''' Swarm Parameters '''
        self.numbers = 100
        self.max_velocity = 0.5         # m/sec

        self.r_r = 0.6                  # m
        self.r_o = 0.5                  # m
        self.r_a = 1.8                  # m
        self.field_of_perception = 90   # degree

        self.area_width = 5            # m
        self.area_height = 5            # m
        self.margin = 0.05

        self.repulsion_gain = .2
        self.attraction_gain = 0.1
        self.orientation_gain = 0.4

        ''' Initial values'''
        self.position = self.numbers * [[0, 0]]
        self.heading = self.numbers * [0]
        self.heading_vel = self.numbers * [0]
        self.heading_acc = self.numbers * [0]
        self.velocity = self.numbers * [self.max_velocity]
        self.influence_vector_x = self.numbers * [0]
        self.influence_vector_y = self.numbers * [0]
        self.density = self.numbers / (self.area_width * self.area_height)  # agent / m^2




def nothing(x):
    pass


def px(point):
    pixel_scalar = 1500
    point_in_pixel = point * pixel_scalar
    return point_in_pixel


def clamp(n, minn, maxn):
    return max(min(maxn, n), minn)


def distance(p, q):
    xi = swarm.position[p][0]
    yi = swarm.position[p][1]
    xii = swarm.position[q][0]
    yii = swarm.position[q][1]
    sq1 = (xi - xii) ** 2
    sq2 = (yi - yii) ** 2
    return sqrt(sq1 + sq2)


def is_in_perception_field(p, q):
    alpha = 0.3333333333
    position_vector_x = swarm.position[q][0] - swarm.position[p][0]
    position_vector_y = swarm.position[q][1] - swarm.position[p][1]
    position_vector_abs = sqrt(position_vector_x**2 + position_vector_y**2)
    if position_vector_abs > 0:
        alpha = acos((position_vector_x * cos(swarm.heading[p]) + position_vector_y * sin(swarm.heading[p])) / position_vector_abs)

    if alpha < 0.5 * swarm.field_of_perception:
        return True
    else:
        return False


def repulsive_boundaries_pt(p):

    x = 0
    y = 0
    s = 500
    K_b = 0.1

    N = swarm.position[p][0] - swarm.margin

    x = x + K_b * (pi / 2 - atan(N * s))

    N = (swarm.area_width - swarm.margin) - swarm.position[p][0]
    x = x - K_b * (pi / 2 - atan(N * s))

    N = swarm.position[p][1] - swarm.margin
    y = y + K_b * (pi / 2 - atan(N * s))

    N = (swarm.area_width - swarm.margin) - swarm.position[p][1]
    y = y - K_b * (pi / 2 - atan(N * s))

    ptx = [x, y]
    return ptx



def rotate_list(l, x):
    return l[-x:] + l[:-x]


N = 100
file = open('swarm_simulation.txt', 'w')
file.write('agent_numbers' + ' ')
file.write('density' + ' ')
file.write('velocity' + ' ')
file.write('pressure' + ' ')

s_r = 5000
s_a = 5000
s_o = 5000



K_r = 0.0
K_a = 0.0
K_o = 0.0

if is_real:
    K_r = 0.05
    K_a = 0.002
    K_o = 0.000




for Temp in range(sample_T):

    for Num in range(sample_N):

        print('T = %d,    N = %d' % (Temp, Num))

        swarm = Swarm()
        swarm.numbers = Num_from + Num*int((Num_to - Num_from)/sample_N)
        swarm.max_velocity = Temp_from + Temp*((Temp_to - Temp_from)/sample_T)


        ''' Initial values'''
        swarm.position = swarm.numbers * [[0, 0]]
        swarm.heading = swarm.numbers * [0]
        swarm.heading_vel = swarm.numbers * [0]
        swarm.heading_acc = swarm.numbers * [0]

        swarm.influence_vector_x = swarm.numbers * [0]
        swarm.influence_vector_y = swarm.numbers * [0]
        swarm.density = swarm.numbers / (swarm.area_width * swarm.area_height)  # agent / m^2



        ''' Initial random values for positions and headings '''
        for i in range(swarm.numbers):
            swarm.heading[i] = random() * 2*pi
            swarm.position[i] = [random()*(swarm.area_width - 2 * swarm.margin)+swarm.margin, random()*(swarm.area_height - 2 * swarm.margin)+swarm.margin]

        if show_on:
            width = px(swarm.area_width)
            height = px(swarm.area_height)
            cv2.namedWindow("swarm", cv2.WINDOW_NORMAL)
            cv2.createTrackbar('r_r', 'swarm', 0, swarm.area_width*100, nothing)
            cv2.createTrackbar('r_o', 'swarm', 0, swarm.area_width*100, nothing)
            cv2.createTrackbar('r_a', 'swarm', 0, swarm.area_width*100, nothing)
            cv2.createTrackbar('field_of_perception', 'swarm', 0, 360, nothing)
            cv2.setTrackbarPos('r_r', 'swarm', int(swarm.r_r*100))
            cv2.setTrackbarPos('r_o', 'swarm', int(swarm.r_o*100))
            cv2.setTrackbarPos('r_a', 'swarm', int(swarm.r_a*100))




        sample_counter = 0
        bound_effect_stored = 0



        while 1:

            sample_counter += 1

            if sample_counter > simulation_steps:
                break

            if show_on:
                img = np.ones((height, width, 3), np.uint8) * 255
                swarm.r_r = cv2.getTrackbarPos('r_r', 'swarm') / 100.00
                swarm.r_o = cv2.getTrackbarPos('r_o', 'swarm') / 100.00
                swarm.r_a = cv2.getTrackbarPos('r_a', 'swarm') / 100.00
                swarm.field_of_perception = cv2.getTrackbarPos('field_of_perception', 'swarm')




            swarm.influence_vector_x = swarm.numbers * [0]
            swarm.influence_vector_y = swarm.numbers * [0]


            bound_effect = 0

            for p in range(swarm.numbers):

                pt_r = [0, 0]
                pt_a = [0, 0]
                pt_o = [0, 0]
                pt = [0, 0]
                T = 0
                if is_real:
                    for neighbor in range(swarm.numbers):
                        if p != neighbor:
                            d = distance(p, neighbor)
                            r = [swarm.position[neighbor][0] - swarm.position[p][0], swarm.position[neighbor][1] - swarm.position[p][1]]

                            temp_r = atan((d - swarm.r_r) * s_r)
                            pt_r[0] = (-1) * K_r * (r[0]/d) * (pi/2 - temp_r)
                            pt_r[1] = (-1) * K_r * (r[1]/d) * (pi/2 - temp_r)

                            temp_a = atan((d - swarm.r_a)*s_a)
                            pt_a[0] = K_a * (r[0]/d) * (pi/2 - temp_a)
                            pt_a[1] = K_a * (r[1]/d) * (pi/2 - temp_a)

                            # pt_o[0] = K_o * cos(swarm.heading[neighbor]) * (pi/2 - atan((d-swarm.r_o)*s_o))
                            # pt_o[1] = K_o * sin(swarm.heading[neighbor]) * (pi/2 - atan((d-swarm.r_o)*s_o))

                            pt[0] += pt_r[0] + pt_a[0] + pt_o[0]
                            pt[1] += pt_r[1] + pt_a[1] + pt_o[1]

                            swarm.influence_vector_x[neighbor] += pt[0]
                            swarm.influence_vector_y[neighbor] += pt[1]


                bound_pt = repulsive_boundaries_pt(p)
                pt[0] = pt[0] + bound_pt[0]
                pt[1] = pt[1] + bound_pt[1]

                bound_effect += sqrt(bound_pt[0]**2 + bound_pt[1]**2)

                N = swarm.position[p][1] - swarm.margin
                # y = y + K_b * (pi / 2 - atan(N * s))



                while abs(swarm.heading[p]) > pi:
                    if swarm.heading[p] > pi:
                        swarm.heading[p] = swarm.heading[p] - 2*pi
                    if swarm.heading[p] < -pi:
                        swarm.heading[p] = swarm.heading[p] + 2 * pi
                if show_on:
                    cv2.line(img, (int(px(swarm.position[p][0])), int(px(swarm.position[p][1]))),
                             (int(px(swarm.position[p][0]) + 2000*MAG * pt[0]),
                              int(px(swarm.position[p][1]) + 2000*MAG * pt[1])), (160, 23, 180), 6*MAG)

                swarm.heading[p] = atan2(pt[1] + 1.0*sin(swarm.heading[p]), pt[0] + 1.0*cos(swarm.heading[p]))


            bound_effect_stored += bound_effect
            bound_effect_avg = bound_effect_stored / (sample_counter * 2*(swarm.area_height + swarm.area_width))
            swarm.density = swarm.numbers / (swarm.area_width * swarm.area_height)
            k_b = bound_effect_avg/(swarm.max_velocity * swarm.density)


            if show_on:
                for i in range(swarm.numbers):

                    # cv2.line(img, (int(px(swarm.position[i][0])), int(px(swarm.position[i][1]))),
                    #          (int(px(swarm.position[i][0]) + 100 * swarm.influence_vector_x[i]),
                    #           int(px(swarm.position[i][1]) + 100 * swarm.influence_vector_y[i])), (200, 230, 20))
    ################################################################################################################################################
                    cv2.line(img, (int(px(swarm.position[i][0])), int(px(swarm.position[i][1]))),
                            (int(px(swarm.position[i][0]) + cos(swarm.heading[i])*60*MAG) , int(px(swarm.position[i][1]) + sin(swarm.heading[i])*60*MAG)), (20, 20, 20), 6*MAG)
                    cv2.circle(img, (int(px(swarm.position[i][0])), int(px(swarm.position[i][1]))), 20*MAG, (20, 20, 20), 5*MAG)


            ''' Update positions based on KINEMATIC/DYNAMIC model'''
            ''' Here we use a simple "x = x0 + v*t" kinematic model'''

            for i in range(swarm.numbers):
                swarm.position[i][0] += swarm.max_velocity * dt * cos(swarm.heading[i])
                swarm.position[i][1] += swarm.max_velocity * dt * sin(swarm.heading[i])

            if show_on:
                cv2.imshow("swarm", img)
                cv2.waitKey(1)



        file.write('\n')

        file.write(str(swarm.numbers) + ' ')
        file.write(str(swarm.density) + ' ')
        file.write(str(swarm.max_velocity) + ' ')
        file.write(str(bound_effect_avg) + ' ')

file.close()

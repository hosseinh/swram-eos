clc
close all
%clear all
%uiimport('swarm_simulation_ideal_50_5000.txt')

sigma = zeros(25,1);
k = zeros(2500,200);
M = zeros(200,1);
STD = zeros(200,1);
k_s = 0.033;

for alpha=1:1:200
    for i=1:2500
        T = velocity(i).^(alpha/100);
        P = pressure(i);
        rho = density(i);
        
        k(i,alpha) = P / (rho * T);
    
    end
    
     M(alpha) = mean(k(:,alpha));
     STD(alpha) = std(k(:,alpha));

end


a = linspace(0.1,2,200);

% for i=1:10:2500    
% plot(a, k(i,:), 'LineWidth',0.3,'Color',[0.8 0.8 0.8])        
% end
plot(a, M, 'LineWidth', 1, 'Color', [0.3 0.3 0.3])
hold on
grid on
plot(a, M + STD, 'LineWidth', 1, 'Color', [0.2 0.4 0.8], 'LineStyle', '-.')
plot(a, M - STD, 'LineWidth', 1, 'Color', [0.2 0.4 0.8], 'LineStyle', '-.')

xlabel('\alpha')
ylabel('k_s constant')
ylim([0 0.07]);

% grid on

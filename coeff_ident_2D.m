clc 
close all
%clear all
%uiimport('swarm_simulation_50_real_2000.txt')

n = 50; %   # OF DENSITY SAMPLES
m = 50; %   # OF VELOCITY SAMPLES
k_H = 0.0330;
coeff_numbers = 4;
RHO = zeros(n,coeff_numbers);
D = zeros(n,m);

% CONVERT POINT COULD DATA TO MATRIX DATA
P = v2m(pressure, n);
D = density(1:n);
V = linspace (velocity(1),velocity(n*m),m);

% CREATING RHO MATRIX
for i=1:n
    for j=1:coeff_numbers
        RHO(i,j) = D(i)^j;
    end
end
    
for j=1:m
    for i=1:n
       D(i,j) = b(P(i,j),D(i),V(j));
    end
end

A = RHO\D;

r = linspace(2,5.92,50);
t = linspace(0.2,1.7680,50);

surf(t(2:50),r,D(:,2:50))
%contourf(t(2:50),r,D(:,2:50))
xlabel('T (velocity) (m/s)');
ylabel('\rho (density) (agent/m^2)');
zlabel('\psi (residual term)')




% P_ideal = k_H .* density .* velocity.^0.87;
% P_real = ((RHO * A) + ones(n,1)) .* k_H .* density .* velocity.^0.87;
% P_residual = P_real - P_ideal;
% 
% scatter(density,pressure,'*')
% hold on
% plot(density,P_real,'Color',[.216, .494, .722])
% plot(density,P_ideal,'Color',[.894, .102, .11])
% plot(density,P_residual,'Color',[.302, .686, .29])
% xlabel('density(agent/m^2)')
% ylabel('Presuure(1/s.m)')
% grid on

